def leer_valores():
    fin = False
    valores = []
    while not fin:
        valor = input("introduce un valor para calcular o 'fin' para terminar: ")
        if valor != 'fin':
            valores.append(int(valor))
        else:
            fin = True
    return valores

def calcular_valores(valores):
    for valor in valores:
        iva = (valor * 21) / 100
        print("Valor introducido {0}, iva {1}, total {2}".format(valor, iva, valor + iva)) 

calcular_valores(leer_valores())
